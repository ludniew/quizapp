import typing
import datetime

from web_sockets.tasks import run_quiz
from web_sockets.services.queries import get_quiz_questions, get_participants
from web_sockets.events_handler.visitor import Visitor
from foundation.constants import QUESTION_REVIEW_STAGE_TIME


class EventsHandler(Visitor):
    def visit_Join(self, _) -> None:
        self._visit_player_event(event="PlayerJoined")

    def visit_LoadHistory(self, _) -> None:
        if not self.cache_manager.is_quiz_in_progress():
            self.current_event = "WaitStageStarted"
            content = None

        else:
            content = self._get_content_after_reload()

        self.channel_manager.send_json(
            payload={
                "content": content,
                "eventType": self.current_event,
            }
        )

    def visit_StartQuiz(self, _) -> None:
        self.cache_manager.build_room_cache_basement()
        questions = get_quiz_questions(self.room_id)
        users = get_participants(self.room_id)
        self.cache_manager.setup_scores_for_users(questions=questions, users=users)
        run_quiz.delay(
            questions=questions,
            room_id=self.room_id,
            room_group_name=self.room_group_name,
            channel_name=self.channel_manager.channel_name,
        )

    def visit_Answer(self, payload) -> None:
        if not payload:
            self.visit_Error({"message": "Answer data was not provided"})

        elif not self.cache_manager.is_quiz_in_progress():
            self.visit_Error({"message": "Quiz has not started yet"})

        elif not self.cache_manager.is_answering_allowed():
            self.visit_Error(
                {"message": "Answering is not allowed in current quiz stage"}
            )

        else:
            room_data = self.cache_manager.get_room_data()
            scores_data = room_data["scores"]
            scores_question_data = scores_data[self.user.pk][payload["questionId"]]

            users_data = room_data["users"]
            users_data[self.user] = payload

            scores_question_data["answers"] = payload["answers"]
            scores_question_data["answer_time"] = self._get_time_passed_since_timestamp(
                timestamp_type="start_question_answer_stage_timestamp"
            )

            self.cache_manager.set_room_data(value=room_data)

    def visit_PlayerDisconnected(self, _) -> None:
        self._visit_player_event(event="PlayerDisconnected")

    def visit_Error(self, payload) -> None:
        self.channel_manager.send_json({"error": payload["message"]})

    def _visit_player_event(self, event):
        content = get_participants(self.room_id)
        self.current_event = event

        self.channel_manager.send_to_group(
            event=event, payload={"currentPlayers": len(content)}
        )

    def _get_content_after_reload(self) -> typing.Dict[str, str]:
        room_data = self.cache_manager.get_room_data()
        quiz_data = room_data["quiz"]
        user_data = room_data["users"]
        question = quiz_data["question"]

        content = {
            "userAnswers": user_data.get(self.user, {}),
            "question": self._get_question_content(question),
        }
        match quiz_data["quiz_stage"]:
            case "QuestionReviewStageStarted":
                content.update(
                    {
                        "correctAnswers": question["correct_answers"],
                        "feedback": question["feedback"],
                    }
                )
                self.current_event = "QuestionReviewStageReloaded"

            case "QuestionAnswerStageStarted":
                self.current_event = "QuestionAnswerStageStarted"

        content["timeRemaining"] = self._get_time_remaining()
        return content

    def _get_time_remaining(self) -> float:
        match self.current_event:
            case "QuestionAnswerStageStarted":
                return (
                    self._get_time_to_answer_from_cache()
                    - self._get_time_passed_since_timestamp(
                        timestamp_type="start_question_answer_stage_timestamp"
                    )
                )

            case "QuestionReviewStageReloaded":
                return (
                    QUESTION_REVIEW_STAGE_TIME
                    - self._get_time_passed_since_timestamp(
                        timestamp_type="start_question_review_stage_timestamp"
                    )
                )

    def _get_time_passed_since_timestamp(self, timestamp_type: str) -> float:
        room_data = self.cache_manager.get_room_data()
        timestamps_data = room_data["timestamps"]
        start_timestamp = timestamps_data[timestamp_type]
        remaining_time = datetime.datetime.now() - start_timestamp

        return float(f"{remaining_time.seconds}.{str(remaining_time.microseconds)[0]}")

    def _get_time_to_answer_from_cache(self) -> float:
        quiz_data = self.cache_manager.get_room_data()
        return quiz_data["quiz"]["question"]["time_to_answer"]

    def _get_question_content(
        self,
        question,
    ) -> typing.Dict[str, str]:
        return {
            "id": question["id"],
            "question": question["text"],
            "answers": question["answers"],
            "type": question["type"],
            "timeToAnswer": question["time_to_answer"],
            "progress": question["progress"],
        }
