from web_sockets.channel_manager import ChannelManager

from cache import CacheManager
from users.models import User


class Visitor:
    def __init__(
        self,
        room_id: int,
        room_group_name: str,
        user: User,
        channel_name: str,
    ):
        self.room_id = room_id
        self.room_group_name = room_group_name
        self.user = user
        self.current_event = None

        self.channel_manager = ChannelManager(
            room_group_name=room_group_name, channel_name=channel_name
        )
        self.cache_manager = CacheManager(room_id=self.room_id)

    def visit(self, action: str, payload: dict) -> None:
        method_name = f"visit_{action}"
        event = getattr(self, method_name, None)
        if event is None:
            event = self.default_visit
        event(payload)

    def default_visit(self, _) -> None:
        self.visit(
            action="Error",
            payload={"message": "Event is not allowed"},
        )
