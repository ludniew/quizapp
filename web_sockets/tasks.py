import typing

from celery import shared_task

from web_sockets.quiz_performance import QuizPerformance


@shared_task
def run_quiz(
    questions: typing.List[typing.Dict[str, any]],
    room_id: int,
    room_group_name: str,
    channel_name: str,
) -> None:
    QuizPerformance(
        questions=questions,
        room_id=room_id,
        room_group_name=room_group_name,
        channel_name=channel_name,
    ).run()
