from django.contrib import admin

from web_sockets.models import QuizRoom, UserQuizRoom

admin.site.register(QuizRoom)
admin.site.register(UserQuizRoom)
