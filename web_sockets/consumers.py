import typing

from asgiref.sync import sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer

from web_sockets.events_handler import Visitor
from web_sockets.events_handler import EventsHandler
from web_sockets.services.queries import (
    connect_user,
    disconnect_user,
)


class QuizConsumer(AsyncJsonWebsocketConsumer):
    def __init__(self, *args, **kwargs) -> None:
        self.user = None
        self.room_id = None
        self.room_group_name = None
        self.event_handler: Visitor | None = None
        super().__init__(*args, **kwargs)

    async def connect(self) -> None:
        self.user = self.scope["user"]
        self.room_id = int(self.scope["url_route"]["kwargs"]["room_id"])
        self.room_group_name = f"room_{self.room_id}"
        self.event_handler = EventsHandler(
            room_id=self.room_id,
            room_group_name=self.room_group_name,
            user=self.user,
            channel_name=self.channel_name,
        )

        await connect_user(user=self.user, room_id=self.room_id)
        await self.channel_layer.group_add(self.room_group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code) -> None:
        if close_code != 4999:
            await disconnect_user(self.user, room_id=self.room_id),
            await sync_to_async(self.event_handler.visit)(
                action="PlayerDisconnected", payload=None
            )
            await self.channel_layer.group_discard(
                self.room_group_name, self.channel_name
            )

    async def send_message(self, request, close=False) -> None:
        await super().send_json(content=request["payload"], close=close)

    async def receive_json(self, request: typing.Dict[str, any], **kwargs) -> None:
        try:
            event = request["eventType"]
        except KeyError:
            await sync_to_async(self.event_handler.visit)(
                action="Error", payload={"message": "Event type was not provided"}
            )

        else:
            await sync_to_async(self.event_handler.visit)(
                action=event, payload=request.get("data", None)
            )
