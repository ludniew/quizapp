from django.urls import re_path

from web_sockets.consumers import QuizConsumer


ws_urlpatterns = [re_path(r"quiz/(?P<room_id>\w+)/$", QuizConsumer.as_asgi())]
