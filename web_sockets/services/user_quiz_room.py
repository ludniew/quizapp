from users.models import User
from web_sockets.models import UserQuizRoom, QuizRoom
from foundation.utils.decorators import ignore_does_not_exist_exception


def connect_user_to_quiz_room(user: User, quiz_room: QuizRoom) -> None:
    UserQuizRoom.objects.get_or_create(user=user, quiz_room=quiz_room)


@ignore_does_not_exist_exception
def disconnect_user_from_quiz_room(user: User, quiz_room: QuizRoom) -> None:
    UserQuizRoom.objects.get(user=user, quiz_room=quiz_room).delete()
