import typing

from channels.db import database_sync_to_async

from users.models import User
from web_sockets.models import UserQuizRoom
from auth.services import get_authenticated_user
from quiz.services.decorators import (
    overwrite_correct_answers_for_specific_type,
    shuffle_answers,
)
from quiz.services import get_active_quiz, get_quiz_room_by_id, get_quiz_room_by_quiz
from web_sockets.services.user_quiz_room import (
    connect_user_to_quiz_room,
    disconnect_user_from_quiz_room,
)


@database_sync_to_async
def connect_user(user: User, room_id: int) -> None:
    quiz = get_active_quiz(code=room_id)
    quiz_room, _ = get_quiz_room_by_quiz(quiz=quiz)
    connect_user_to_quiz_room(user=user, quiz_room=quiz_room)


def get_participants(room_id: int) -> typing.List[str]:
    quiz_room = get_quiz_room_by_id(room_id=room_id)
    return [
        user_quiz_room.user
        for user_quiz_room in UserQuizRoom.objects.filter(quiz_room=quiz_room)
    ]


@database_sync_to_async
def disconnect_user(user: User, room_id: int) -> None:
    quiz_room = get_quiz_room_by_id(room_id=room_id)
    disconnect_user_from_quiz_room(user=user, quiz_room=quiz_room)


@database_sync_to_async
def get_user(access_token: str) -> User:
    return get_authenticated_user(access_token)


@overwrite_correct_answers_for_specific_type
@shuffle_answers(question_types=("CHR",))
def get_quiz_questions(room_id: int) -> typing.List[typing.Dict[str, any]]:
    quiz = get_active_quiz(code=room_id)
    return [
        {
            "id": question.id,
            "text": question.text,
            "answers": [
                {
                    "id": str(answer.uuid),
                    "content": answer.content,
                }
                for answer in question.answers
            ],
            "correct_answers": [
                str(answer.uuid) for answer in question.correct_answers
            ],
            "feedback": question.feedback,
            "time_to_answer": question.time,
            "type": question.type,
        }
        for question in sorted(quiz.questions, key=lambda x: x.position)
    ]
