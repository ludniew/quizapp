from urllib.parse import parse_qs

from web_sockets.services.queries import get_user
from web_sockets.exceptions import AuthorizationCodeNotProvided


class QueryAuthMiddleware:
    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):
        params = parse_qs(scope["query_string"])
        try:
            access_token = params[b"access"][0]
        except KeyError:
            raise AuthorizationCodeNotProvided(
                "'access' query param was not provided in the request"
            )

        scope["user"] = await get_user(access_token)

        return await self.app(scope, receive, send)
