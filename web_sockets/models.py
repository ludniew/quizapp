from django.db import models

from quiz.models import Quiz
from users.models import User


class QuizRoom(models.Model):
    quiz = models.OneToOneField(
        Quiz,
        on_delete=models.CASCADE,
        primary_key=True,
    )


class UserQuizRoom(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    quiz_room = models.ForeignKey(QuizRoom, on_delete=models.CASCADE)

    @property
    def user_email(self):
        return self.user.email
