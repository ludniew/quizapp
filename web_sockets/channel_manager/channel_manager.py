from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer


class ChannelManager:
    def __init__(self, room_group_name: str, channel_name: str):
        self.room_group_name = room_group_name
        self.channel_name = channel_name
        self.channel_layer = get_channel_layer()

    def send_to_group(self, event, payload: any = None) -> None:
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                "type": "send_message",
                "payload": {
                    "content": payload,
                    "eventType": event,
                },
            },
        )

    def send_json(self, payload: any) -> None:
        async_to_sync(self.channel_layer.send)(
            self.channel_name,
            {"type": "send_message", "payload": payload},
        )
