import time
import typing
import datetime

from cache import CacheManager
from web_sockets.channel_manager import ChannelManager
from quiz.services.quizzes import get_active_quiz
from scores.services.scores import get_podium_scores, delete_scores
from scores.services.scores_computing import ScoresComputing
from foundation.constants import QUESTION_REVIEW_STAGE_TIME


class QuizPerformance:
    def __init__(
        self,
        questions: typing.List[typing.Dict[str, any]],
        room_id: int,
        room_group_name: str,
        channel_name: str,
    ):
        self._questions = questions
        self._room_id = room_id
        self._room_group_name = room_group_name
        self._cache_manager = CacheManager(room_id=room_id)
        self._channel_manager = ChannelManager(
            room_group_name=room_group_name, channel_name=channel_name
        )
        self._computing_unit = ScoresComputing(room_id=self._room_id)
        self._scores = None

    def run(self) -> None:
        try:
            self._perform_quiz_setup()
            for index, question in enumerate(self._questions, start=1):
                self._perform_question_setup()
                # Question answer stage
                question["progress"] = f"{index}/{len(self._questions)}"
                self._perform_question_answer_stage(question=question)

                # Question review stage
                self._perform_question_review_stage(question=question)

            else:
                # Quiz finished
                self._send_quiz_finish_event()
        finally:
            self._perform_quiz_teardown()

    def _perform_question_setup(self):
        room_data = self._cache_manager.get_room_data()
        room_data["users"] = {}

        self._cache_manager.set_room_data(value=room_data)

    def _perform_question_answer_stage(self, question: typing.Dict[str, any]) -> None:
        content = {
            "question": {
                "id": question["id"],
                "question": question["text"],
                "progress": question["progress"],
                "answers": question["answers"],
                "type": question["type"],
                "timeToAnswer": question["time_to_answer"],
            },
        }
        event_type = "QuestionAnswerStageStarted"

        self._set_current_quiz_stage_to_cache(quiz_stage=event_type)
        self._set_question_content_to_cache(question),
        self._set_start_question_answer_stage_timestamp_in_cache(),
        self._channel_manager.send_to_group(event=event_type, payload=content)
        time.sleep(question["time_to_answer"]),

    def _perform_question_review_stage(self, question: typing.Dict[str, any]) -> None:
        content = {
            "correctAnswers": question["correct_answers"],
            "feedback": question["feedback"],
        }
        event_type = "QuestionReviewStageStarted"

        self._set_current_quiz_stage_to_cache(quiz_stage=event_type)
        self._set_start_question_review_stage_timestamp_in_cache()
        self._channel_manager.send_to_group(event=event_type, payload=content)
        time.sleep(QUESTION_REVIEW_STAGE_TIME)

    def _set_question_content_to_cache(self, question: typing.Dict[str, any]) -> None:
        room_data = self._cache_manager.get_room_data()
        room_data["quiz"]["question"] = question

        self._cache_manager.set_room_data(value=room_data)

    def _set_start_question_answer_stage_timestamp_in_cache(self) -> None:
        room_data = self._cache_manager.get_room_data()
        room_data["timestamps"][
            "start_question_answer_stage_timestamp"
        ] = datetime.datetime.now()

        self._cache_manager.set_room_data(value=room_data)

    def _set_start_question_review_stage_timestamp_in_cache(self) -> None:
        room_data = self._cache_manager.get_room_data()
        room_data["timestamps"][
            "start_question_review_stage_timestamp"
        ] = datetime.datetime.now()

        self._cache_manager.set_room_data(value=room_data)

    def _set_current_quiz_stage_to_cache(self, quiz_stage: str) -> None:
        room_data = self._cache_manager.get_room_data()
        room_data["quiz"]["quiz_stage"] = quiz_stage

        self._cache_manager.set_room_data(value=room_data)

    def _perform_quiz_setup(self):
        delete_scores(quiz=get_active_quiz(code=self._room_id))
        room_data = self._cache_manager.get_room_data()
        room_data["quiz"]["in_progress"] = True
        room_data["quiz"]["quiz_stage"] = "QuizStarted"

        self._cache_manager.set_room_data(value=room_data)

    def _perform_quiz_teardown(self) -> None:
        room_data = self._cache_manager.get_room_data()

        if room_data:
            room_data["quiz"]["in_progress"] = False
            room_data["quiz"]["quiz_stage"] = "QuizFinished"

            self._cache_manager.set_room_data(value=room_data)

    def _send_quiz_finish_event(self) -> None:
        self._compute_scores()
        content = [
            {"email": score.user.email, "score": score.score}
            for score in get_podium_scores(room_id=self._room_id)
        ]
        self._channel_manager.send_to_group(event="QuizFinished", payload=content)

    def _compute_scores(self) -> None:
        room_data = self._cache_manager.get_room_data()
        self._computing_unit.run(scores=room_data["scores"])
