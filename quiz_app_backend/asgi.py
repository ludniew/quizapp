import os

from django.core.asgi import get_asgi_application

from channels.routing import ProtocolTypeRouter
from channels.routing import URLRouter
from channels.auth import AuthMiddlewareStack

from web_sockets.middlewares import QueryAuthMiddleware
from web_sockets.ws_urls import ws_urlpatterns

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "quiz_app_backend.settings.local")

django_asgi_app = get_asgi_application()

application = ProtocolTypeRouter(
    {
        "http": django_asgi_app,
        "websocket": QueryAuthMiddleware(
            AuthMiddlewareStack(URLRouter(ws_urlpatterns))
        ),
    }
)
