from django.contrib import admin
from django.urls import path, include

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny

schema_view = get_schema_view(
    openapi.Info(
        title="QuizApp API",
        default_version="1.0.0",
        description="QuizApp API documentation",
    ),
    permission_classes=[AllowAny],
    public=True,
)

urlpatterns = [
    path("docs/", schema_view.with_ui("swagger")),
    path("admin/", admin.site.urls),
    path("api/v1/auth/", include("auth.urls")),
    path("api/v1/users/", include("users.urls")),
    path("api/v1/quizzes/", include("quiz.urls")),
    path("api/v1/scores/", include("scores.urls")),
]
