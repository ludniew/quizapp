from functools import wraps
from django.core.exceptions import ObjectDoesNotExist


def ignore_does_not_exist_exception(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ObjectDoesNotExist:
            pass

    return wrapper
