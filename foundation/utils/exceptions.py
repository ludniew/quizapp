from rest_framework.exceptions import APIException


class OperationNotPermitted(APIException):
    status_code = 403
    default_detail = "Operation is not permitted"
