from auth.authentications import JWTCustomAuthentication
from users.models import User


def get_authenticated_user(access_token: str) -> User:
    authentication = JWTCustomAuthentication()
    validated_token = authentication.get_validated_token(raw_token=access_token)

    user = authentication.get_user(validated_token=validated_token)
    return user
