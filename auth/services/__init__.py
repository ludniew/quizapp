from auth.services.auth import get_authenticated_user


__all__ = ["get_authenticated_user"]
