from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.exceptions import InvalidToken


class JWTCustomAuthentication(JWTAuthentication):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_validated_token(self, *args, **kwargs):
        try:
            return super().get_validated_token(*args, **kwargs)

        except InvalidToken:
            raise InvalidToken(
                detail="Token is invalid or expired",
            )
