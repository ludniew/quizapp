from django.core.cache import cache


class Redis:
    def __init__(self):
        self._cache = cache

    def set_key(self, key, value, timeout=None) -> None:
        self._cache.set(key, value, timeout=timeout)

    def get_key(self, key) -> any:
        return self._cache.get(key)

    def delete_key(self, key) -> bool:
        return self._cache.delete(key)

    def __contains__(self, item):
        return item in self._cache
