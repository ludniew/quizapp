from cache.redis import redis_instance


class CacheManager:
    def __init__(self, room_id):
        self._room_id = room_id
        self._cache = redis_instance

    def is_quiz_in_progress(self):
        if room := self.get_room_data():
            if quiz := room.get("quiz"):
                return quiz.get("in_progress")

    def is_answering_allowed(self):
        if room := self.get_room_data():
            if quiz := room.get("quiz"):
                return quiz.get("quiz_stage") == "QuestionAnswerStageStarted"

    def get_room_data(self):
        return self._cache.get_key(self._room_id)

    def set_room_data(self, value):
        self._cache.set_key(key=self._room_id, value=value)

    def build_room_cache_basement(self):
        self._cache.set_key(
            key=self._room_id,
            value={
                "quiz": {"quiz_stage": "QuizStarted"},
                "users": {},
                "timestamps": {},
                "scores": {},
            },
        ),

    def setup_scores_for_users(self, questions, users) -> None:
        room_data = self.get_room_data()

        for user in users:
            room_data["scores"][user.id] = {
                question["id"]: {
                    "type": question["type"],
                    "correct_answers": question["correct_answers"],
                    "answers": [],
                    "time_to_answer": question["time_to_answer"],
                    "answer_time": None,
                }
                for question in questions
            }
        self._cache.set_key(self._room_id, room_data)
