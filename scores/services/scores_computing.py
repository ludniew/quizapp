import typing

from quiz.services.quizzes import get_quiz_room_by_id
from scores.services.scores import create_score
from users.services.users import get_user_by_id


class ScoresComputing:
    def __init__(self, room_id: int):
        self._room_id = room_id
        self._quiz_room = get_quiz_room_by_id(self._room_id)

    def run(
        self, scores: typing.Dict[int, typing.Dict[int, typing.Dict[str, any]]]
    ) -> None:
        for user_id, user_score in scores.items():
            score = sum(
                [
                    _compute_question_points(question_details)
                    for question_id, question_details in user_score.items()
                ]
            )

            create_score(
                user=get_user_by_id(user_id=user_id),
                quiz=self._quiz_room.quiz,
                score=f"{round(score * 100)}",
            )


def _compute_question_points(question_score: typing.Dict[str, any]) -> int or float:
    add_points = False

    match question_score["type"]:
        case "SC" | "MC" | "TF":
            add_points = _get_SC_MC_TF_criteria_status(question_score)

        case "CHR":
            add_points = _get_CHR_criteria_status(question_score)

    if question_score["answer_time"] and add_points:
        return 1 + (
            1 - 1 * (question_score["answer_time"] / question_score["time_to_answer"])
        )
    return 0


def _get_SC_MC_TF_criteria_status(question_score: typing.Dict[str, any]) -> bool:
    return sorted(question_score["answers"]) == sorted(
        question_score["correct_answers"]
    )


def _get_CHR_criteria_status(question_score: typing.Dict[str, any]) -> bool:
    return question_score["answers"] == question_score["correct_answers"]
