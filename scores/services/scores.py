import typing

from users.models import User
from quiz.models import Quiz
from scores.models import Score
from quiz.services.quizzes import get_quiz_room_by_id


def create_score(user: User, quiz: Quiz, score: str) -> None:
    Score.objects.create(user=user, quiz=quiz, score=score)


def delete_scores(quiz: Quiz) -> None:
    quiz.scores.delete()


def get_podium_scores(room_id: int) -> typing.List[Score]:
    return get_scores(room_id=room_id)[:3]


def get_scores(room_id: int) -> typing.List[Score]:
    quiz_room = get_quiz_room_by_id(room_id=room_id)
    return sorted(
        [score for score in Score.objects.filter(quiz=quiz_room.quiz)],
        key=lambda score: float(score.score),
        reverse=True,
    )
