from django.db import models

from quiz.models import Quiz, TimeTrack
from users.models import User


class Score(TimeTrack):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    score = models.CharField(max_length=128)
