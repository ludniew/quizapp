from django.urls import path

from scores.views import ListScoresAPIView, ClearScoresAPIView

urlpatterns = [
    path("", ListScoresAPIView.as_view(), name="score-list"),
    path("<int:code>/", ClearScoresAPIView.as_view(), name="score-delete"),
]
