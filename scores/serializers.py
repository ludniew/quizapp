from rest_framework import serializers

from scores.models import Score
from quiz.serializers import BaseQuizSerializer
from users.serializers import UserSerializer


class ListScoreSerializer(serializers.ModelSerializer):
    quiz = BaseQuizSerializer()

    class Meta:
        model = Score
        fields = ["score", "quiz"]


class AdminListScoreSerializer(ListScoreSerializer):
    user = UserSerializer()

    class Meta:
        model = Score
        fields = ["user", "score", "quiz"]
