from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response

from scores.models import Score
from quiz.services.quizzes import get_quiz
from scores.services.scores import delete_scores
from scores.serializers import ListScoreSerializer, AdminListScoreSerializer
from foundation.utils.exceptions import OperationNotPermitted


class ListScoresAPIView(generics.ListAPIView):
    def get_queryset(self):
        user = self.request.user

        if user.is_superuser:
            return Score.objects.all()

        return Score.objects.filter(user=user)

    def get_serializer_class(self):
        if self.request.user.is_superuser:
            return AdminListScoreSerializer

        return ListScoreSerializer


class ClearScoresAPIView(APIView):
    def delete(self, request: Request, code: int, format=None):
        if not request.user.is_superuser:
            raise OperationNotPermitted()

        quiz = get_quiz(code=code)
        delete_scores(quiz=quiz)

        return Response(
            status=status.HTTP_204_NO_CONTENT,
        )
