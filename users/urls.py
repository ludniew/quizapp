from django.urls import path

from users.views import RetrieveCurrentUserDataAPIView, StaffUsersListAPIView

urlpatterns = [
    path("me/", RetrieveCurrentUserDataAPIView.as_view(), name="user-data"),
    path("staff/", StaffUsersListAPIView.as_view(), name="staff-user-list"),
]
