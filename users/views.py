from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.request import Request
from rest_framework.response import Response
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from users.models import User
from users.serializers import StaffUserSerializer


class RetrieveCurrentUserDataAPIView(APIView):
    @swagger_auto_schema(
        responses={
            200: openapi.Response(
                "Current User data",
                openapi.Schema(
                    title="User",
                    type=openapi.TYPE_OBJECT,
                    properties={
                        "email": openapi.Schema(
                            title="Email address", type=openapi.TYPE_STRING
                        ),
                        "is_admin": openapi.Schema(
                            title="Is admin", type=openapi.TYPE_BOOLEAN
                        ),
                        "first_name": openapi.Schema(
                            title="First name", type=openapi.TYPE_STRING
                        ),
                        "last_name": openapi.Schema(
                            title="Last name", type=openapi.TYPE_STRING
                        ),
                    },
                ),
            )
        }
    )
    def get(self, request: Request, format=None):
        return Response(
            data={
                "email": request.user.email,
                "is_admin": request.user.is_staff,
                "first_name": request.user.first_name,
                "last_name": request.user.last_name,
            },
            status=status.HTTP_200_OK,
        )


class StaffUsersListAPIView(generics.ListAPIView):
    serializer_class = StaffUserSerializer

    def get_queryset(self):
        return User.get_admins()
