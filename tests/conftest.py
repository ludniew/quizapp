import pytest
from unittest.mock import Mock, patch
from rest_framework.test import APIClient
from pytest_factoryboy import register
from tests.factories import (
    UserFactory,
    GroupFactory,
    CategoryFactory,
    QuizFactory,
    QuestionFactory,
    AnswerFactory,
    QuizRoomFactory,
    UserQuizRoomFactory,
    ScoreFactory,
)

from channels.db import database_sync_to_async

from tests.fixtures.web_sockets.communicators import CustomWebsocketCommunicator
from tests.fixtures.web_sockets.channel_manager import channel_manager_mock
from tests.fixtures.cache.cache_manager import get_cache_manager
from web_sockets.consumers import QuizConsumer
from web_sockets.events_handler import EventsHandler

register(UserFactory)
register(GroupFactory)
register(CategoryFactory)
register(QuizFactory)
register(QuestionFactory)
register(AnswerFactory)
register(QuizRoomFactory)
register(UserQuizRoomFactory)
register(ScoreFactory)


@pytest.fixture
def client():
    client = APIClient()
    return client


@pytest.fixture
def authenticated_client(user_factory):
    user = user_factory.create()
    client = APIClient()
    client.force_authenticate(user=user)
    return {"client": client, "user": user}


@pytest.fixture
def authenticated_client_superuser(user_factory):
    user = user_factory.create(is_superuser=True)
    client = APIClient()
    client.force_authenticate(user=user)
    return client


@pytest.fixture
@database_sync_to_async
def user_async(user_factory):
    return user_factory.create()


@pytest.fixture
@database_sync_to_async
def quiz_async(quiz_factory):
    return quiz_factory.create()


@pytest.fixture
@database_sync_to_async
def active_quiz_async(quiz_factory):
    return quiz_factory.create(is_active=True)


@pytest.fixture
async def communicator_async(active_quiz_async, user_async):
    quiz = await active_quiz_async
    user = await user_async
    comm = CustomWebsocketCommunicator(
        application=QuizConsumer.as_asgi(),
        path=f"quiz/{quiz.code}/",
        user=user,
    )
    return comm


@pytest.fixture
@pytest.mark.django_db
def event_handler(user_quiz_room_factory):
    user_quiz_room = user_quiz_room_factory.create()

    with patch(
        "web_sockets.events_handler.visitor.ChannelManager",
        Mock(return_value=channel_manager_mock),
    ):
        handler = EventsHandler(
            room_id=user_quiz_room.quiz_room.quiz.code,
            room_group_name=f"room_{user_quiz_room.quiz_room.quiz.code}",
            user=user_quiz_room.user,
            channel_name="some_channel",
        )
        handler.cache_manager = get_cache_manager(
            room_id=user_quiz_room.quiz_room.quiz.code
        )

    return handler
