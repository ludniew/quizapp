from unittest.mock import Mock


channel_manager_mock = Mock()
channel_manager_mock.send_to_group = Mock()
channel_manager_mock.send_json = Mock()
