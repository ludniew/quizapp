class Redis:
    def __init__(self):
        self._engine = {}

    def set_key(self, key, value):
        self._engine[key] = value

    def get_key(self, key):
        return self._engine[key]
