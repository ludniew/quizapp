from cache.cache_manager import CacheManager
from tests.fixtures.cache.redis import Redis


def get_cache_manager(room_id) -> CacheManager:
    cache_manager = CacheManager(room_id=room_id)
    cache_manager._cache = Redis()

    return cache_manager
