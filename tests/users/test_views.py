import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_GET_RetrieveCurrentUserDataAPIView_should_return_current_user_data(
    authenticated_client,
):
    user = authenticated_client["user"]
    target_url = reverse("user-data")

    response = authenticated_client["client"].get(target_url)
    response_data = response.data

    assert response.status_code == 200
    assert response_data["email"] == user.email
    assert response_data["is_admin"] is False
    assert response_data["first_name"] == user.first_name
    assert response_data["last_name"] == user.last_name


@pytest.mark.django_db
def test_GET_StaffUsersListAPIView_should_return_staff_users(
    authenticated_client, user_factory
):
    staff_users = [user_factory.create(is_staff=True).email for _ in range(3)]
    regular_users = [user_factory.create(is_staff=False).email for _ in range(2)]
    target_url = reverse("staff-user-list")

    response = authenticated_client["client"].get(target_url)
    response_data = response.data

    assert response.status_code == 200
    assert len(response_data) == len(staff_users)
    for user in response_data:
        assert user["email"] in staff_users
        assert user["email"] not in regular_users
