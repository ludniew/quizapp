import pytest
from faker import Faker

from quiz.models import Quiz
from quiz.services import ensure_quiz_exist
from quiz.services.decorators import (
    shuffle_answers,
    overwrite_correct_answers_for_specific_type,
)
from quiz.services.exceptions import QuizDoesNotExist

fake = Faker()


@pytest.mark.django_db
def test_ensure_quiz_exist_raises_exception_when_quiz_does_not_exist():
    invalid_quiz_id = fake.random_int()

    @ensure_quiz_exist
    def some_logic():
        Quiz.objects.get(id=invalid_quiz_id)

    with pytest.raises(QuizDoesNotExist, match="Quiz does not exist"):
        some_logic()


@pytest.mark.django_db
def test_ensure_quiz_exist_passes_when_quiz_exists(quiz_factory):
    quiz = quiz_factory.create()

    @ensure_quiz_exist
    def some_logic():
        Quiz.objects.get(id=quiz.id)

    some_logic()


@pytest.mark.django_db
def test_when_shuffle_answers_decorator_with_question_type_provided_then_input_is_shuffled(
    question_factory, answer_factory
):
    question = question_factory.create(type="CHR")
    for _ in range(5):
        answer_factory.create(question=question)
    answers = [answer.pk for answer in question.answers]
    questions_input = [
        {
            "id": question.pk,
            "answers": answers[:],
            "type": question.type,
        }
    ]

    @shuffle_answers(question_types=("CHR",))
    def answers_after_decoration(questions):
        return questions

    result = answers_after_decoration(questions=questions_input)

    assert [
        answer["id"] for question in result for answer in question["answers"]
    ] != answers


@pytest.mark.django_db
def test_when_shuffle_answers_decorator_with_question_type_not_provided_then_input_is_not_shuffled(
    question_factory, answer_factory
):
    question = question_factory.create()
    for _ in range(5):
        answer_factory.create(question=question)
    answers = [answer.pk for answer in question.answers]
    questions_input = [
        {
            "answers": answers[:],
            "type": question.type,
        }
    ]

    @shuffle_answers()
    def answers_after_decoration(questions):
        return questions

    result = answers_after_decoration(questions=questions_input)

    assert [answer for question in result for answer in question["answers"]] == answers


@pytest.mark.django_db
@pytest.mark.parametrize("question_type", ["SC", "MC", "TF"])
def test_overwrite_correct_answers_for_each_type_should_return_correct_answers_for_SC_MC_TF(
    question_type, question_factory, answer_factory
):
    question = question_factory.create()
    for _ in range(5):
        answer_factory.create(question=question, is_correct=False)
    correct_answer = answer_factory.create(question=question, is_correct=True)
    questions_input = [
        {
            "correct_answers": [answer.pk for answer in question.correct_answers],
            "type": question_type,
        }
    ]

    @overwrite_correct_answers_for_specific_type
    def some_logic(questions):
        return questions

    result = some_logic(questions=questions_input)

    assert [
        answer for question in result for answer in question["correct_answers"]
    ] == [correct_answer.pk]


@pytest.mark.django_db
def test_overwrite_correct_answers_for_each_type_should_overwrite_correct_answers_for_CHR(
    question_factory, answer_factory
):
    question = question_factory.create()
    for index, _ in enumerate(range(5), start=1):
        answer_factory.create(
            question=question, is_correct=False, chronology_order=index
        )
    question_answers = [str(answer.uuid) for answer in question.answers]
    chronology_sorted_answers = sorted(
        [answer for answer in question.answers], key=lambda x: x.chronology_order
    )
    question_input = [
        {
            "id": question.id,
            "correct_answers": [
                str(answer.uuid) for answer in question.correct_answers
            ],
            "type": "CHR",
        }
    ]

    @overwrite_correct_answers_for_specific_type
    def some_logic(questions):
        return questions

    result = some_logic(questions=question_input)

    assert [
        answer for question in result for answer in question["correct_answers"]
    ] == question_answers
    assert [
        answer for question in result for answer in question["correct_answers"]
    ] == [str(answer.uuid) for answer in chronology_sorted_answers]
