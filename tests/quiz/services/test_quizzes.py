import pytest
from faker import Faker

from quiz.models import Quiz
from quiz.services.quizzes import create_quiz

fake = Faker()


@pytest.mark.django_db
def test_create_quiz_when_quiz_instance_provided_then_quiz_is_built(
    user_factory, group_factory, category_factory
):
    user = user_factory.create()
    category = category_factory.create()
    group = group_factory.create()
    answer = {
        "chronology_order": fake.random_int(min=1, max=10),
        "content": fake.text(),
        "is_correct": fake.boolean(),
    }
    question = {
        "position": fake.random_int(),
        "text": fake.text(),
        "time": fake.random_int(min=5, max=60),
        "image_src": fake.file_path(extension="png"),
        "feedback": fake.text(),
        "type": "SC",
        "group": group,
        "answers": [answer],
    }
    validated_data = {
        "name": fake.word(),
        "description": fake.text(),
        "image_src": fake.file_path(extension="png"),
        "category": category,
        "created_by": user.pk,
        "questions": [question],
    }

    quiz = create_quiz(validated_data=validated_data, instance=None)
    quiz_db = Quiz.objects.get(id=quiz.pk)
    question_db = quiz_db.questions[0]
    answer_db = question_db.answers[0]

    assert quiz_db.name == validated_data["name"]
    assert quiz_db.description == validated_data["description"]
    assert quiz_db.image_src == validated_data["image_src"]
    assert quiz_db.category == validated_data["category"]
    assert quiz_db.created_by == user
    assert question_db.position == question["position"]
    assert question_db.text == question["text"]
    assert question_db.time == question["time"]
    assert question_db.image_src == question["image_src"]
    assert question_db.feedback == question["feedback"]
    assert question_db.type == question["type"]
    assert question_db.group == question["group"]
    assert answer_db.chronology_order == answer["chronology_order"]
    assert answer_db.content == answer["content"]
    assert answer_db.is_correct == answer["is_correct"]
