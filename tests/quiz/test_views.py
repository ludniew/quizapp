import pytest
from unittest.mock import Mock, patch
from django.urls import reverse

from tests.fixtures.cache.cache_manager import get_cache_manager

from quiz import models


@pytest.mark.django_db
def test_GET_QuizListCreateAPIView_should_returns_quizzes(
    authenticated_client, quiz_factory
):
    quiz = quiz_factory.create()
    target_url = reverse("quiz-list-create")

    response = authenticated_client["client"].get(
        target_url,
    )
    response_data = response.data[0]

    assert response.status_code == 200
    assert response_data["id"] == quiz.id
    assert response_data["name"] == quiz.name
    assert response_data["description"] == quiz.description
    assert response_data["category"] == quiz.category.id
    assert response_data["image_src"] == quiz.image_src
    assert response_data["is_active"] is False
    assert response_data["created_by"] == quiz.created_by.email


@pytest.mark.django_db
def test_POST_QuizListCreateAPIView_should_create_quiz_with_questions_and_answers(
    authenticated_client, user_factory, category_factory, group_factory
):
    user = user_factory.create()
    category = category_factory.create()
    group = group_factory.create()
    target_url = reverse("quiz-list-create")

    payload = {
        "name": "Hobbit quiz",
        "description": "quiz desc",
        "imageSrc": "images/image.png",
        "createdBy": user.email,
        "category": category.id,
        "questions": [
            {
                "position": 1,
                "text": "text",
                "imageSrc": None,
                "time": 20,
                "feedback": "feedback",
                "type": "SC",
                "group": group.id,
                "answers": [
                    {"content": "answer 1", "chronologyOrder": 1, "isCorrect": False},
                    {"content": "answer 2", "chronologyOrder": 2, "isCorrect": True},
                ],
            }
        ],
    }
    question = payload["questions"][0]
    answer = question.copy().pop("answers")[0]

    response = authenticated_client["client"].post(
        target_url, data=payload, format="json"
    )
    response_data = response.data
    quiz_db = models.Quiz.objects.all()[0]
    question_db = quiz_db.question_set.all()[0]
    answer_db = question_db.answer_set.all()[0]

    assert response.status_code == 201
    assert response_data["id"] == quiz_db.id
    assert response_data["is_active"] is False
    assert question["position"] == question_db.position
    assert question["text"] == question_db.text
    assert question["imageSrc"] == question_db.image_src
    assert question["time"] == question_db.time
    assert question["feedback"] == question_db.feedback
    assert question["type"] == question_db.type
    assert question["group"] == question_db.group.id
    assert answer["content"] == answer_db.content
    assert answer["chronologyOrder"] == answer_db.chronology_order
    assert answer["isCorrect"] == answer_db.is_correct


@pytest.mark.django_db
def test_PUT_QuizRetrieveUpdateDestroyAPIView_should_update_quiz_with_new_data(
    authenticated_client, quiz_factory, category_factory, group_factory
):
    category = category_factory.create()
    group = group_factory.create()
    quiz = quiz_factory.create()
    target_url = reverse("quiz-retrieve-update-delete", kwargs={"pk": quiz.id})

    payload = {
        "name": "Hobbit quiz",
        "description": "quiz desc",
        "imageSrc": "images/image.png",
        "category": category.id,
        "questions": [
            {
                "position": 1,
                "text": "text",
                "imageSrc": None,
                "time": 20,
                "feedback": "feedback",
                "type": "SC",
                "group": group.id,
                "answers": [
                    {"content": "answer 1", "chronologyOrder": 1, "isCorrect": False},
                    {"content": "answer 2", "chronologyOrder": 2, "isCorrect": True},
                ],
            }
        ],
    }
    question = payload["questions"][0]
    answer_1, answer_2 = question.copy().pop("answers")
    quiz_db_before_update = models.Quiz.objects.get(id=quiz.id)

    response = authenticated_client["client"].put(
        f"{target_url}", data=payload, format="json"
    )

    updated_quiz_db = models.Quiz.objects.all()[0]
    question_db = updated_quiz_db.question_set.all()[0]
    answers_db = question_db.answer_set.all()
    answer_db_1 = answers_db[0]
    answer_db_2 = answers_db[1]

    assert response.status_code == 200
    assert updated_quiz_db.id == quiz_db_before_update.id
    assert updated_quiz_db.created_by == quiz_db_before_update.created_by
    assert updated_quiz_db.code == quiz_db_before_update.code
    assert question["position"] == question_db.position
    assert question["text"] == question_db.text
    assert question["imageSrc"] == question_db.image_src
    assert question["time"] == question_db.time
    assert question["feedback"] == question_db.feedback
    assert question["type"] == question_db.type
    assert question["group"] == question_db.group.id
    assert answer_1["content"] == answer_db_1.content
    assert answer_1["chronologyOrder"] == answer_db_1.chronology_order
    assert answer_1["isCorrect"] == answer_db_1.is_correct
    assert answer_2["content"] == answer_db_2.content
    assert answer_2["chronologyOrder"] == answer_db_2.chronology_order
    assert answer_2["isCorrect"] == answer_db_2.is_correct


@pytest.mark.django_db
def test_GET_QuizStartAPIView_should_make_quiz_active(
    authenticated_client, quiz_factory
):
    quiz = quiz_factory.create()
    target_url = reverse("quiz-start", kwargs={"pk": quiz.id})

    assert quiz.is_active is False

    authenticated_client["client"].get(target_url, format="json")
    quiz_db = models.Quiz.objects.get(id=quiz.id)

    assert quiz_db.is_active is True


@pytest.mark.django_db
def test_GET_QuizStopAPIView_should_make_quiz_inactive(
    authenticated_client, quiz_factory
):
    quiz = quiz_factory.create()
    target_url = reverse("quiz-stop", kwargs={"pk": quiz.id})
    quiz.is_active = True
    quiz.save()

    authenticated_client["client"].get(target_url, format="json")
    quiz_db = models.Quiz.objects.get(id=quiz.id)

    assert quiz_db.is_active is False


@pytest.mark.django_db
def test_GET_CategoryListCreateAPIView_should_return_list_of_categories(
    authenticated_client, category_factory
):
    category = category_factory.create()
    target_url = reverse("category-list-create")

    response = authenticated_client["client"].get(target_url)
    category_data = response.data[0]

    assert response.status_code == 200
    assert category_data["name"] == category.name
    assert category_data["id"] == category.id


@pytest.mark.django_db
def test_POST_CategoryListCreateAPIView_should_create_category(authenticated_client):
    target_url = reverse("category-list-create")
    categories_initial = models.Category.objects.all()
    payload = {"name": "some_category"}
    assert list(categories_initial) == []

    response = authenticated_client["client"].post(target_url, data=payload)
    response_data = response.data
    category_db = models.Category.objects.get(id=response_data["id"])

    assert response.status_code == 201
    assert category_db.id == response_data["id"]
    assert category_db.name == payload["name"]


@pytest.mark.django_db
def test_GET_GroupListCreateAPIView_should_return_list_of_groups(
    authenticated_client, group_factory
):
    group = group_factory.create()
    target_url = reverse("group-list-create")

    response = authenticated_client["client"].get(target_url)
    group_data = response.data[0]

    assert response.status_code == 200
    assert group_data["name"] == group.name
    assert group_data["id"] == group.id


@pytest.mark.django_db
def test_POST_GroupListCreateAPIView_should_create_group(authenticated_client):
    target_url = reverse("group-list-create")
    groups_initial = models.Group.objects.all()
    payload = {"name": "some_group"}
    assert list(groups_initial) == []

    response = authenticated_client["client"].post(target_url, data=payload)
    response_data = response.data
    group_db = models.Group.objects.get(id=response_data["id"])

    assert response.status_code == 201
    assert group_db.id == response_data["id"]
    assert group_db.name == payload["name"]


@pytest.mark.django_db
def test_POST_QuizActivityCheck_should_return_status_200_if_quiz_active_and_not_in_progress(
    authenticated_client, quiz_factory
):
    quiz = quiz_factory.create()
    quiz.is_active = True
    quiz.save()
    cache_manager = get_cache_manager(quiz.code)
    cache_manager.set_room_data(value={"quiz": {"in_progress": False}})
    target_url = reverse("quiz-activity-check")
    payload = {"code": quiz.code}

    with patch("quiz.services.quizzes.CacheManager", Mock(return_value=cache_manager)):
        response = authenticated_client["client"].post(target_url, data=payload)

    assert response.status_code == 200


@pytest.mark.django_db
def test_POST_QuizActivityCheck_should_return_status_200_if_quiz_active_and_in_progress_and_user_was_in_lobby(
    authenticated_client, quiz_factory
):
    quiz = quiz_factory.create()
    quiz.is_active = True
    quiz.save()
    cache_manager = get_cache_manager(quiz.code)
    cache_manager.set_room_data(
        value={
            "quiz": {"in_progress": True},
            "scores": {authenticated_client["user"].pk: {}},
        }
    )
    target_url = reverse("quiz-activity-check")
    payload = {"code": quiz.code}

    with patch("quiz.services.quizzes.CacheManager", Mock(return_value=cache_manager)):
        response = authenticated_client["client"].post(target_url, data=payload)

    assert response.status_code == 200


@pytest.mark.django_db
def test_POST_QuizActivityCheck_should_return_status_406_if_quiz_active_and_in_progress_and_user_was_not_in_lobby(
    authenticated_client, quiz_factory
):
    quiz = quiz_factory.create()
    quiz.is_active = True
    quiz.save()
    cache_manager = get_cache_manager(quiz.code)
    cache_manager.set_room_data(value={"quiz": {"in_progress": True}, "scores": {}})
    target_url = reverse("quiz-activity-check")
    payload = {"code": quiz.code}

    with patch("quiz.services.quizzes.CacheManager", Mock(return_value=cache_manager)):
        response = authenticated_client["client"].post(target_url, data=payload)

    assert response.status_code == 406


@pytest.mark.django_db
def test_POST_QuizActivityCheck_should_return_status_403_if_quiz_inactive(
    authenticated_client, quiz_factory
):
    quiz = quiz_factory.create()
    target_url = reverse("quiz-activity-check")
    payload = {"code": quiz.code}

    response = authenticated_client["client"].post(target_url, data=payload)

    assert response.status_code == 403
    assert response.data["detail"] == f"Quiz with code: {quiz.code} has not started yet"


@pytest.mark.django_db
def test_POST_QuizActivityCheck_should_return_status_404_if_quiz_does_not_exist(
    authenticated_client,
):
    target_url = reverse("quiz-activity-check")
    payload = {"code": "123456"}

    response = authenticated_client["client"].post(target_url, data=payload)

    assert response.status_code == 404
    assert response.data["detail"] == "Quiz does not exist"
