import pytest
from unittest.mock import Mock


@pytest.mark.django_db
@pytest.mark.parametrize(
    "action",
    [
        "Error",
        "Join",
        "LoadHistory",
        "StartQuiz",
        "Answer",
        "PlayerDisconnected",
    ],
)
def test_event_handler_visit_should_call_action_correctly(event_handler, action):
    action_mock = Mock()
    payload = None

    setattr(event_handler, f"visit_{action}", action_mock)
    event_handler.visit(action=action, payload=payload)

    action_mock.assert_called_once_with(payload)


@pytest.mark.django_db
def test_event_handler_when_event_not_allowed_then_default_visit_called(event_handler):
    action = "NotAllowedAction"
    expected_payload = {"message": "Event is not allowed"}

    event_handler.visit_Error = Mock()
    event_handler.visit(action=action, payload=None)

    event_handler.visit_Error.assert_called_once_with(expected_payload)


@pytest.mark.django_db
def test_event_handler_visit_Join_should_call_channel_layer_with_correct_payload(
    event_handler,
):
    action = "Join"
    expected_current_event = "PlayerJoined"
    expected_payload = {"currentPlayers": 1}

    event_handler.visit(action=action, payload=None)

    assert event_handler.current_event == expected_current_event
    event_handler.channel_manager.send_to_group.assert_called_once_with(
        event=expected_current_event, payload=expected_payload
    )
