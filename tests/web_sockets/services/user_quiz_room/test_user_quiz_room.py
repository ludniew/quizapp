import pytest

from web_sockets.models import UserQuizRoom
from web_sockets.services.user_quiz_room import (
    connect_user_to_quiz_room,
    disconnect_user_from_quiz_room,
)


@pytest.mark.django_db
def test_connect_user_to_quiz_room_should_create_user_quiz_room_relation(
    quiz_room_factory, user_factory
):
    user = user_factory.create()
    quiz_room = quiz_room_factory.create()

    connect_user_to_quiz_room(user=user, quiz_room=quiz_room)
    user_quiz_room_db = UserQuizRoom.objects.get(user=user, quiz_room=quiz_room)

    assert user_quiz_room_db


@pytest.mark.django_db
def test_connect_user_to_quiz_room_should_get_user_quiz_room_if_exist(
    user_quiz_room_factory,
):
    user_quiz_room = user_quiz_room_factory.create()

    connect_user_to_quiz_room(
        user=user_quiz_room.user, quiz_room=user_quiz_room.quiz_room
    )
    user_quiz_room_db = UserQuizRoom.objects.all()

    assert len(user_quiz_room_db) == 1


@pytest.mark.django_db
def test_disconnect_user_from_quiz_room_should_delete_user_room_quiz_relation(
    user_quiz_room_factory,
):
    user_quiz_room = user_quiz_room_factory.create()

    disconnect_user_from_quiz_room(
        user=user_quiz_room.user, quiz_room=user_quiz_room.quiz_room
    )
    user_quiz_room_db = UserQuizRoom.objects.all()

    assert len(user_quiz_room_db) == 0
