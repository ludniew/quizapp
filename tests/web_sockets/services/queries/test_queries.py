import pytest
from asgiref.sync import sync_to_async

from web_sockets.services.queries import (
    connect_user,
    get_participants,
    disconnect_user,
    get_quiz_questions,
)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_connect_disconnect_user_should_add_remove_user_from_room(
    user_async, active_quiz_async
):
    user = await user_async
    quiz = await active_quiz_async

    await connect_user(user=user, room_id=quiz.code)

    assert len(await sync_to_async(get_participants)(quiz.code)) == 1

    await disconnect_user(user=user, room_id=quiz.code)

    assert len(await sync_to_async(get_participants)(quiz.code)) == 0


@pytest.mark.django_db
def test_get_quiz_questions_should_return_questions_content(
    question_factory, quiz_factory, answer_factory
):
    quiz = quiz_factory.create(is_active=True)
    question = question_factory.create(quiz=quiz, type="SC")
    answer = answer_factory.create(question=question)

    answers = [answer]

    expected = [
        {
            "id": question.id,
            "text": question.text,
            "answers": [
                {
                    "id": answer.uuid,
                    "content": answer.content,
                }
            ],
            "correct_answers": [answer.uuid for answer in answers if answer.is_correct],
            "feedback": question.feedback,
            "time_to_answer": question.time,
            "type": question.type,
        }
    ]

    questions = get_quiz_questions(quiz.code)

    assert questions == expected
