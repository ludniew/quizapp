import pytest


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_consumer_when_connecting_to_web_socket_then_result_is_True(
    communicator_async,
):
    communicator = await communicator_async
    connected, _ = await communicator.connect()

    assert connected

    await communicator.disconnect()


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_consumer_when_sending_message_to_web_socket_then_result_received_as_dict(
    communicator_async,
):
    communicator = await communicator_async

    await communicator.connect()
    await communicator.send_json_to(data={"eventType": "Join"})
    response = await communicator.receive_json_from()
    await communicator.disconnect()

    assert response == {"eventType": "PlayerJoined", "content": {"currentPlayers": 1}}
