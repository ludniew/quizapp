import pytest

from scores.models import Score
from scores.services.scores import (
    create_score,
    get_scores,
    get_podium_scores,
    delete_scores,
)


@pytest.mark.django_db
def test_create_score_should_create_score_instance(user_factory, quiz_factory):
    user = user_factory.create()
    quiz = quiz_factory.create()
    score = "1.23"

    create_score(user=user, quiz=quiz, score=score)
    score_db = Score.objects.all().first()

    assert score_db.score == score
    assert score_db.user == user
    assert score_db.quiz == quiz


@pytest.mark.django_db
def test_get_scores_should_return_sorted_scores(score_factory, quiz_room_factory):
    quiz_room = quiz_room_factory.create()
    scores = [score_factory.create(quiz=quiz_room.quiz) for _ in range(5)]

    result = get_scores(room_id=quiz_room.quiz.code)

    assert sorted(scores, key=lambda score: float(score.score), reverse=True) == result


@pytest.mark.django_db
def test_get_podium_scores_should_return_sorted_podium_scores(
    score_factory, quiz_room_factory
):
    quiz_room = quiz_room_factory.create()
    scores = [score_factory.create(quiz=quiz_room.quiz) for _ in range(5)]

    result = get_podium_scores(room_id=quiz_room.quiz.code)

    assert (
        sorted(scores, key=lambda score: float(score.score), reverse=True)[:3] == result
    )


@pytest.mark.django_db
def test_get_podium_scores_should_return_sorted_podium_scores_even_if_less_then_3_participants(
    score_factory, quiz_room_factory
):
    quiz_room = quiz_room_factory.create()
    scores = [score_factory.create(quiz=quiz_room.quiz) for _ in range(1)]

    result = get_podium_scores(room_id=quiz_room.quiz.code)

    assert (
        sorted(scores, key=lambda score: float(score.score), reverse=True)[:3] == result
    )


@pytest.mark.django_db
def test_delete_scores_should_delete_all_scores_related_to_quiz(
    score_factory, quiz_factory
):
    quiz = quiz_factory.create()
    scores = [score_factory.create(quiz=quiz) for _ in range(10)]
    assert len(Score.objects.all()) == len(scores)

    delete_scores(quiz=quiz)

    assert len(Score.objects.all()) == 0
