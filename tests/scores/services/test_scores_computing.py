import pytest

from scores.models import Score
from scores.services.scores_computing import ScoresComputing, _compute_question_points


@pytest.mark.django_db
def test_computing_run_should_create_score_with_correct_parameters(quiz_room_factory):
    quiz_room = quiz_room_factory.create()
    q1_time_to_answer = 60
    q1_answer_time = 9.5
    q2_time_to_answer = 20
    q2_answer_time = 1.1
    scores = {
        1: {
            1: {
                "type": "SC",
                "correct_answers": [1],
                "answers": [1],
                "time_to_answer": q1_time_to_answer,
                "answer_time": q1_answer_time,
            },
            2: {
                "type": "SC",
                "correct_answers": [2],
                "answers": [2],
                "time_to_answer": q2_time_to_answer,
                "answer_time": q2_answer_time,
            },
        }
    }
    q1_score = 1 + (1 - 1 * (q1_answer_time / q1_time_to_answer))
    q2_score = 1 + (1 - 1 * (q2_answer_time / q2_time_to_answer))
    expected_score = f"{round((q1_score + q2_score) * 100)}"

    ScoresComputing(room_id=quiz_room.quiz.code).run(scores=scores)
    score = Score.objects.all().first()

    assert score.score == expected_score


@pytest.mark.parametrize(
    "answers, correct_answers, question_type",
    [
        ([], [1], "SC"),
        ([1], [1, 2], "MC"),
        ([2, 1], [1, 2, 3], "TF"),
    ],
)
def test__compute_question_when_SC_MC_TF_and_incorrect_answers_then_should_return_zero_points(
    answers, correct_answers, question_type
):
    question_score = {
        "type": question_type,
        "correct_answers": correct_answers,
        "answers": answers,
        "time_to_answer": 30,
        "answer_time": 10,
    }
    points = _compute_question_points(question_score)

    assert points == 0


@pytest.mark.parametrize(
    "answers, correct_answers, question_type",
    [
        ([1], [1], "SC"),
        ([2, 1], [1, 2], "MC"),
        ([2, 3, 1], [1, 2, 3], "TF"),
    ],
)
def test__compute_question_when_SC_MC_TF_and_correct_answers_then_should_return_points(
    answers, correct_answers, question_type
):
    question_score = {
        "type": question_type,
        "correct_answers": correct_answers,
        "answers": answers,
        "time_to_answer": 30,
        "answer_time": 20,
    }
    points = _compute_question_points(question_score)

    assert points != 0


@pytest.mark.parametrize(
    "answers, correct_answers",
    [
        ([1], [1, 2]),
        ([2, 1], [1, 2]),
        ([2, 3, 1], [1, 2, 3]),
    ],
)
def test__compute_question_when_CHR_and_incorrect_answers_then_should_return_zero_points(
    answers, correct_answers
):
    question_score = {
        "type": "CHR",
        "correct_answers": correct_answers,
        "answers": answers,
        "time_to_answer": 30,
        "answer_time": 20,
    }
    points = _compute_question_points(question_score)

    assert points == 0


@pytest.mark.parametrize(
    "answers, correct_answers",
    [
        ([1, 2], [1, 2]),
        ([2, 1], [2, 1]),
        ([1, 2, 3], [1, 2, 3]),
    ],
)
def test__compute_question_when_CHR_and_correct_answers_then_should_return_points(
    answers, correct_answers
):
    question_score = {
        "type": "CHR",
        "correct_answers": correct_answers,
        "answers": answers,
        "time_to_answer": 30,
        "answer_time": 20,
    }
    points = _compute_question_points(question_score)

    assert points != 0
