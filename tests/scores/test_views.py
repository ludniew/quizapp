import pytest
from django.urls import reverse

from scores.models import Score


@pytest.mark.django_db
def test_GET_ListScoresAPIView_should_return_nested_quiz(
    authenticated_client, score_factory
):
    target_url = reverse("score-list")
    score_factory.create(user=authenticated_client["user"])

    result = authenticated_client["client"].get(path=target_url)
    result_data = result.data[0]
    score_db = Score.objects.all()[0]

    assert result.status_code == 200
    assert score_db.score == result_data["score"]
    assert score_db.quiz.id == result_data["quiz"]["id"]
    assert score_db.quiz.name == result_data["quiz"]["name"]
    assert score_db.quiz.image_src == result_data["quiz"]["image_src"]
    assert (
        score_db.quiz.created_date.strftime("%Y-%m-%d")
        == result_data["quiz"]["created_date"]
    )


@pytest.mark.django_db
def test_GET_ListScoresAPIView_should_return_all_users_scores_for_superuser(
    authenticated_client_superuser, score_factory
):
    target_url = reverse("score-list")
    scores = [score_factory.create() for _ in range(10)]

    result = authenticated_client_superuser.get(path=target_url)

    assert result.status_code == 200
    assert len(result.data) == len(scores)


@pytest.mark.django_db
def test_GET_ListScoresAPIView_should_return_only_user_scores_for_basic_user(
    authenticated_client, score_factory
):
    user = authenticated_client["user"]
    target_url = reverse("score-list")
    user_scores = [score_factory.create(user=user) for _ in range(10)]
    other_scores = [score_factory.create() for _ in range(5)]

    result = authenticated_client["client"].get(path=target_url)

    assert result.status_code == 200
    assert len(result.data) == len(user_scores)
    for score in other_scores:
        assert score not in result.data


@pytest.mark.django_db
def test_DELETE_ClearScoresAPIView_should_return_all_users_scores_for_superuser(
    authenticated_client_superuser, quiz_factory, score_factory
):
    quiz = quiz_factory.create()
    target_url = reverse("score-delete", kwargs={"code": quiz.code})
    scores = [score_factory.create(quiz=quiz) for _ in range(10)]
    assert len(Score.objects.all()) == len(scores)

    result = authenticated_client_superuser.delete(path=target_url)

    assert result.status_code == 204
    assert len(Score.objects.filter(quiz=quiz)) == 0


@pytest.mark.django_db
def test_DELETE_ClearScoresAPIView_should_raise_exception_operation_not_permitted(
    authenticated_client, quiz_factory
):
    quiz = quiz_factory.create()
    target_url = reverse("score-delete", kwargs={"code": quiz.code})

    result = authenticated_client["client"].delete(path=target_url)

    assert result.status_code == 403
    assert result.data["detail"] == "Operation is not permitted"
