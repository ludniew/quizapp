import factory.fuzzy
import factory
from faker import Faker
from django.contrib.auth import get_user_model

from quiz.models import (
    Group,
    Category,
    Quiz,
    Question,
    Answer,
)
from scores.models import Score
from web_sockets.models import QuizRoom, UserQuizRoom

User = get_user_model()
fake = Faker()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Sequence(lambda x: fake.email())
    password = factory.Sequence(lambda x: fake.password())
    is_staff = False
    is_active = True


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Group

    name = factory.Sequence(lambda x: fake.word())


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category

    name = factory.Sequence(lambda x: fake.domain_word())


class QuizFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Quiz

    name = factory.Sequence(lambda x: fake.word())
    description = factory.Sequence(lambda x: fake.text())
    image_src = factory.Sequence(lambda x: fake.file_path(extension="png"))
    code = factory.Sequence(lambda x: fake.random_int(min=100000, max=999999))
    created_by = factory.SubFactory(UserFactory)
    category = factory.SubFactory(CategoryFactory)


class QuestionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Question

    position = factory.Sequence(lambda x: fake.random_int(min=1, max=200))
    text = factory.Sequence(lambda x: fake.text())
    image_src = factory.Sequence(lambda x: fake.file_path(extension="png"))
    time = factory.Sequence(lambda x: fake.random_int(min=1, max=60))
    feedback = factory.Sequence(lambda x: fake.text())
    type = factory.fuzzy.FuzzyChoice(Question.Types.choices, getter=lambda c: c[0])
    quiz = factory.SubFactory(QuizFactory)
    group = factory.SubFactory(GroupFactory)


class AnswerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Answer

    uuid = factory.Sequence(lambda x: fake.uuid4())
    content = factory.Sequence(lambda x: fake.text())
    chronology_order = factory.Sequence(lambda x: fake.random_int(min=1))
    is_correct = factory.Sequence(lambda x: fake.boolean())
    question = factory.SubFactory(QuestionFactory)


class QuizRoomFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = QuizRoom

    quiz = factory.SubFactory(QuizFactory)


class UserQuizRoomFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserQuizRoom

    user = factory.SubFactory(UserFactory)
    quiz_room = factory.SubFactory(QuizRoomFactory)


class ScoreFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Score

    quiz = factory.SubFactory(QuizFactory)
    user = factory.SubFactory(UserFactory)
    score = factory.Sequence(
        lambda x: f"{fake.pyfloat(left_digits=1, right_digits=2, positive=True)}"
    )
