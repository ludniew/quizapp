import datetime
import uuid
import random
from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import gettext_lazy as _


class TimeTrack(models.Model):
    created_date = models.DateField(default=datetime.date.today)
    updated_date = models.DateField(default=datetime.date.today)

    class Meta:
        abstract = True


class Category(TimeTrack):
    name = models.CharField(unique=True, max_length=200)

    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return self.name


class Group(TimeTrack):
    name = models.CharField(unique=True, max_length=200)

    def __str__(self):
        return self.name


class Quiz(TimeTrack):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=400)
    image_src = models.CharField(max_length=256, blank=True, null=True)
    code = models.IntegerField(
        validators=[
            MinValueValidator(limit_value=100000),
            MaxValueValidator(limit_value=999999),
        ],
        unique=True,
    )
    is_active = models.BooleanField(default=False)
    created_by = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING)
    category = models.OneToOneField(Category, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Quizzes"

    @property
    def questions(self):
        return self.question_set.all()

    @property
    def scores(self):
        return self.score_set.all()

    def __str__(self):
        return self.name


class Question(TimeTrack):
    class Types(models.TextChoices):
        SINGLE_CHOICE = "SC", _("single_choice")
        MANY_CHOICE = "MC", _("many_choice")
        TRUE_FALSE = "TF", _("true_false")
        CHRONOLOGY = "CHR", _("chronology")
        MATCHING = "MCH", _("matching")

    position = models.PositiveIntegerField()
    text = models.CharField(max_length=512)
    image_src = models.CharField(max_length=256, blank=True, null=True)
    time = models.PositiveIntegerField()
    feedback = models.CharField(max_length=512)
    type = models.CharField(max_length=30, choices=Types.choices)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    @property
    def answers(self):
        return self.answer_set.all()

    @property
    def correct_answers(self):
        return self.answer_set.filter(is_correct=True)

    def __str__(self):
        return f"Question #{self.id}"


class ShuffledAnswersQuestion(Question):
    class Meta:
        proxy = True

    @property
    def answers(self):
        _answers = list(super().answers)
        random.shuffle(_answers)
        return _answers

    @property
    def correct_answers(self):
        return super().answers


class Answer(TimeTrack):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    content = models.CharField(max_length=256)
    chronology_order = models.PositiveIntegerField()
    is_correct = models.BooleanField()
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
