from rest_framework import serializers

from quiz import models
from quiz import services


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Answer
        fields = ["id", "chronology_order", "content", "is_correct"]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = [
            "id",
            "name",
        ]


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Group
        fields = [
            "id",
            "name",
        ]


class ListQuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(source="answer_set.all", read_only=True, many=True)

    class Meta:
        model = models.Question
        fields = [
            "id",
            "position",
            "text",
            "time",
            "image_src",
            "feedback",
            "type",
            "group",
            "answers",
        ]


class CreateQuestionSerializer(ListQuestionSerializer):
    answers = AnswerSerializer(many=True, write_only=True)


class BaseQuizSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Quiz
        fields = ["id", "name", "image_src", "created_date"]


class BaseExtendedQuizSerializer(serializers.ModelSerializer):
    questions = ListQuestionSerializer(
        source="question_set.all", read_only=True, many=True
    )
    created_date = serializers.DateField(read_only=True, format="%Y-%m-%d")
    code = serializers.IntegerField(read_only=True)

    class Meta:
        model = models.Quiz
        fields = [
            "id",
            "name",
            "description",
            "category",
            "image_src",
            "code",
            "is_active",
            "created_by",
            "created_date",
            "questions",
        ]


class ListQuizSerializer(BaseExtendedQuizSerializer):
    created_by = serializers.SerializerMethodField(read_only=True)

    def get_created_by(self, obj: models.Quiz) -> str:
        return obj.created_by.email


class CreateQuizSerializer(BaseExtendedQuizSerializer):
    questions = CreateQuestionSerializer(many=True, write_only=True)
    created_by = serializers.IntegerField(write_only=True)

    def create(self, validated_data, instance: models.Quiz = None) -> models.Quiz:
        return services.create_quiz(validated_data=validated_data, instance=instance)


class RetrieveDeleteQuizSerializer(ListQuizSerializer):
    pass


class UpdateQuizSerializer(CreateQuizSerializer):
    class Meta:
        model = models.Quiz
        fields = [
            "id",
            "name",
            "description",
            "category",
            "image_src",
            "code",
            "created_date",
            "questions",
        ]

    def update(self, instance: models.Quiz, validated_data) -> models.Quiz:
        return services.update_quiz(instance=instance, validated_data=validated_data)


class QuizVerifyActivitySerializer(serializers.Serializer):
    code = serializers.CharField()
