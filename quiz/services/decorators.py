import typing
from functools import wraps

from rest_framework import status

from quiz.models import Quiz, ShuffledAnswersQuestion
from quiz.services.exceptions import QuizDoesNotExist


def ensure_quiz_exist(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)

        except Quiz.DoesNotExist:
            raise QuizDoesNotExist(
                code=status.HTTP_404_NOT_FOUND, detail="Quiz does not exist"
            )

    return wrapper


def overwrite_correct_answers_for_specific_type(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        questions: typing.List = func(*args, **kwargs)
        for question in questions:
            if question.get("type") == "CHR":
                shuffled_answers_question = ShuffledAnswersQuestion.objects.get(
                    id=question.get("id")
                )
                question["correct_answers"] = [
                    str(answer.uuid)
                    for answer in shuffled_answers_question.correct_answers
                ]

        return questions

    return wrapper


def shuffle_answers(question_types: typing.Tuple[str] = None):
    """Shuffle answers on fly"""

    def decorator(func: typing.Callable[[typing.Any], typing.List]):
        def wrapper(*args, **kwargs):
            questions: typing.List = func(*args, **kwargs)
            for question in questions:
                if question_types and question.get("type") in question_types:
                    shuffled_answers_question = ShuffledAnswersQuestion.objects.get(
                        id=question.get("id")
                    )
                    question["answers"] = [
                        {"id": str(answer.uuid), "content": answer.content}
                        for answer in shuffled_answers_question.answers
                    ]
            return questions

        return wrapper

    return decorator
