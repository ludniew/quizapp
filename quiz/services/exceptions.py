from rest_framework.exceptions import APIException


class QuizDoesNotExist(APIException):
    status_code = 404


class QuizzHasNotStartedYet(APIException):
    status_code = 403


class QuizzHasAlreadyStarted(APIException):
    status_code = 406
