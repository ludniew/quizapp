import datetime
import random
import typing

from users.models import User
from web_sockets.models import QuizRoom
from quiz.models import Quiz, Answer, Question
from quiz.services.decorators import ensure_quiz_exist
from quiz.services.exceptions import QuizzHasNotStartedYet, QuizzHasAlreadyStarted
from cache.cache_manager import CacheManager


def create_quiz(validated_data, instance: typing.Optional[Quiz]) -> Quiz:
    questions = validated_data.pop("questions")

    if not instance:
        validated_data["created_by"] = User.objects.get(id=validated_data["created_by"])

    quiz = instance or Quiz.objects.create(**validated_data, code=_generate_code())

    for question_data in questions:
        answers = question_data.pop("answers")
        question = Question.objects.create(**question_data, quiz=quiz)

        for answer_data in answers:
            Answer.objects.create(**answer_data, question=question)

    return quiz


def update_quiz(instance: Quiz, validated_data) -> Quiz:
    instance.questions.delete()
    instance.name = validated_data["name"]
    instance.description = validated_data["description"]
    instance.category = validated_data["category"]
    instance.image_src = validated_data["image_src"]
    instance.updated_date = datetime.date.today()
    instance.save()

    return create_quiz(validated_data=validated_data, instance=instance)


@ensure_quiz_exist
def get_quiz(code: int) -> Quiz:
    return Quiz.objects.get(code__exact=code)


@ensure_quiz_exist
def start_quiz(quiz_id: int) -> None:
    quiz = Quiz.objects.get(id=quiz_id)
    quiz.is_active = True
    quiz.save()


@ensure_quiz_exist
def stop_quiz(quiz_id: int) -> None:
    quiz = Quiz.objects.get(id=quiz_id)
    quiz.is_active = False
    quiz.save()


@ensure_quiz_exist
def get_active_quiz(code: int) -> Quiz:
    quiz = Quiz.objects.get(code__exact=code)
    if not quiz.is_active:
        raise QuizzHasNotStartedYet(
            detail=f"Quiz with code: {code} has not started yet",
        )

    return quiz


def check_if_user_allowed_to_join(code: int, user: User) -> None:
    cache_manager = CacheManager(room_id=code)
    room_data = cache_manager.get_room_data()

    if cache_manager.is_quiz_in_progress() and user.pk not in room_data["scores"]:
        raise QuizzHasAlreadyStarted(
            detail="Quiz has already started. Please try later"
        )


def get_quiz_room_by_id(room_id: int) -> QuizRoom:
    return QuizRoom.objects.get(quiz__code=room_id)


def get_quiz_room_by_quiz(quiz: Quiz) -> QuizRoom:
    return QuizRoom.objects.get_or_create(quiz=quiz)


def _generate_code() -> int:
    quizzes_codes = [quiz.code for quiz in Quiz.objects.all()]

    while True:
        code = random.randint(100000, 999999)
        if code not in quizzes_codes:
            break

    return code
