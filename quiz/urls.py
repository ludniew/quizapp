from django.urls import path

from quiz import views

urlpatterns = [
    path("", views.QuizListCreateAPIView.as_view(), name="quiz-list-create"),
    path("verify/", views.QuizActivityCheck.as_view(), name="quiz-activity-check"),
    path(
        "<int:pk>/",
        views.QuizRetrieveUpdateDestroyAPIView.as_view(),
        name="quiz-retrieve-update-delete",
    ),
    path(
        "<int:pk>/start/",
        views.QuizStartAPIView.as_view(),
        name="quiz-start",
    ),
    path(
        "<int:pk>/stop/",
        views.QuizStopAPIView.as_view(),
        name="quiz-stop",
    ),
    path(
        "categories/",
        views.CategoryListCreateAPIView.as_view(),
        name="category-list-create",
    ),
    path(
        "categories/<int:pk>/",
        views.CategoryRetrieveUpdateDestroyAPIView.as_view(),
        name="category-get-update-delete",
    ),
    path("groups/", views.GroupListCreateAPIView.as_view(), name="group-list-create"),
    path(
        "groups/<int:pk>/",
        views.GroupRetrieveUpdateDestroyAPIView.as_view(),
        name="groups-get-update-delete",
    ),
]
