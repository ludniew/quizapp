from rest_framework import generics, serializers, status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.request import Request
from drf_yasg.utils import swagger_auto_schema

from quiz import models
from quiz import serializers as serializers_
from quiz.services import (
    start_quiz,
    stop_quiz,
    get_active_quiz,
    check_if_user_allowed_to_join,
)


class QuizListCreateAPIView(generics.ListCreateAPIView):
    queryset = models.Quiz.objects.all()

    def get_serializer(self, *args, **kwargs) -> serializers.ModelSerializer:
        serializer_class = (
            serializers_.ListQuizSerializer
            if self.request.method == "GET"
            else serializers_.CreateQuizSerializer
        )
        return serializer_class(*args, **kwargs)

    def create(self, request: Request, *args, **kwargs) -> models.Quiz:
        try:
            user = models.get_user_model().objects.get(
                email__exact=request.data["created_by"]
            )

        except models.get_user_model().DoesNotExist:
            raise serializers.ValidationError(
                f"User with email '{request.data['created_by']}' does not exist",
                code=status.HTTP_404_NOT_FOUND,
            )

        request.data["created_by"] = user.id
        return super().create(request, *args, **kwargs)


class QuizRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Quiz.objects.all()
    lookup_field = "pk"

    def get_serializer(self, *args, **kwargs) -> serializers.ModelSerializer:
        serializer_class = (
            serializers_.RetrieveDeleteQuizSerializer
            if self.request.method == "GET" or self.request.method == "DELETE"
            else serializers_.UpdateQuizSerializer
        )
        return serializer_class(*args, **kwargs)


class CategoryListCreateAPIView(generics.ListCreateAPIView):
    queryset = models.Category.objects.all()
    serializer_class = serializers_.CategorySerializer


class CategoryRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Category.objects.all()
    serializer_class = serializers_.CategorySerializer
    lookup_field = "pk"


class GroupListCreateAPIView(generics.ListCreateAPIView):
    queryset = models.Group.objects.all()
    serializer_class = serializers_.GroupSerializer


class GroupRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Group.objects.all()
    serializer_class = serializers_.GroupSerializer
    lookup_field = "pk"


class QuizStartAPIView(APIView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: "When Quiz started successfully",
            status.HTTP_404_NOT_FOUND: "When Quiz does noe exist",
        }
    )
    def get(self, _: Request, pk: int, format=None) -> Response:
        start_quiz(quiz_id=pk)
        return Response(
            status=status.HTTP_200_OK,
            data={"status": f"Quiz(id={pk}) has been started"},
        )


class QuizStopAPIView(APIView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: "When Quiz stopped successfully",
            status.HTTP_404_NOT_FOUND: "When Quiz does noe exist",
        }
    )
    def get(self, _: Request, pk: int, format=None) -> Response:
        stop_quiz(quiz_id=pk)
        return Response(
            status=status.HTTP_200_OK, data={"status": f"Quiz(id={pk}) has been ended"}
        )


class QuizActivityCheck(APIView):
    @swagger_auto_schema(
        request_body=serializers_.QuizVerifyActivitySerializer(),
        responses={
            status.HTTP_200_OK: "When Quiz active and User can join",
            status.HTTP_404_NOT_FOUND: "When Quiz does noe exist",
            status.HTTP_403_FORBIDDEN: "When Quiz has not started yet",
            status.HTTP_406_NOT_ACCEPTABLE: "When Quiz has already started and User was not in the lobby",
        },
    )
    def post(self, request, format=None) -> Response:
        serializer = serializers_.QuizVerifyActivitySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        get_active_quiz(**serializer.data)
        check_if_user_allowed_to_join(**serializer.data, user=request.user)
        return Response(status=status.HTTP_200_OK)
